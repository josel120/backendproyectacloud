const mongoose = require('mongoose');

const dataCsvSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    Email: String,
    nombre: String,
    apellido: String,
    Job: String,
    department: String,
    division: String,
    organizacion: String,
    phone_number: String,
    location: String,
    locale: String,
    mail_manager: String,
    id_empleado: String,
    fecha_inicio: String,
    mailCorpo: String
});

module.exports = mongoose.model('dataCsv', dataCsvSchema);