const mongoose = require('mongoose');

const workplaceSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    totalResults: Number,
    itemsPerPage: Number,
    startIndex: Number,
    Resources: JSON
});

module.exports = mongoose.model('workplace', workplaceSchema);