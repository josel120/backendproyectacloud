const express = require('express');
const router = express.Router();
const checkAuth = require('../middleware/check-auth');

const dataCsvController = require("../controllers/dataCsv");


// Ver todos los Usuarios
router.get('/', checkAuth, dataCsvController.dataCsv_verTodo);

// Ingresar Usuario a BD
router.post('/', checkAuth, dataCsvController.dataCsv_addUser);

// Buscar usuario por ID
router.get('/:id', checkAuth, dataCsvController.dataCsv_verById);

// Eliminar usuario por ID
router.delete("/:id", checkAuth, dataCsvController.dataCsv_deleteUser);

// Ruta Patch
router.patch("/:id", checkAuth, dataCsvController.dataCsv_patchUser);

module.exports = router;