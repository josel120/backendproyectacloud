const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/check-auth');
const WorkplaceController = require('../controllers/workplace');

// Listar usuarios Administradores
router.get('/list', checkAuth, WorkplaceController.workplace_user_list);

module.exports = router;