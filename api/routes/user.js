const express = require('express');
const router = express.Router();

const checkAuth = require('../middleware/check-auth');
const UserController = require('../controllers/user');

// Crear nuevo Usuario
router.post('/signup', UserController.user_signup);

// Eliminar Usuario Registrado
router.delete("/:id", checkAuth, UserController.user_delete);

// Login Usuario
router.post('/login', UserController.user_login);

// Listar usuarios Administradores
router.get('/list', checkAuth, UserController.user_list);

module.exports = router;