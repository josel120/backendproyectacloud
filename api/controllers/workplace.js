const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const User = require("../models/workplace");


// Listar usuarios Workplace
exports.workplace_user_list = (req, res, next) => {
    User.find()
    .exec()
    .then(docs => {
        console.log(docs);
        res.status(200).json(docs);
    })  
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};