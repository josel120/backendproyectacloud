const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const dataCsv = require("../models/dataCsv");

// ver todos los usuarios de la BD
exports.dataCsv_verTodo = (req, res, next) => {
    dataCsv.find()
    .exec()
    .then(docs => {
        console.log(docs);
        res.status(200).json(docs);
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

// Ingresar nuevo usuario a la BD
exports.dataCsv_addUser = (req, res, next) => {
    console.log('nuevo',req.body);
    const user = new dataCsv({
        _id: new mongoose.Types.ObjectId(),
        nombre: req.body['First Name'],
        apellido: req.body['Last Name'],
        job: req.body['Job Title'],
        department: req.body.Department,
        division: req.body.Division,
        organizacion: req.body.Organization,
        phone_number: req.body['Phone Number'],
        location: req.body.Location,
        locale: req.body.Locale,
        mail_manager: req.body['Manager Email'],
        id_empleado: req.body['Employee Id'],
        fecha_inicio: req.body['Start Date'],
        mailCorpo: req.body['Mail corporativo'],
    });
    user.save().then(
        result => {
            console.log(result);
            res.status(201).json({
                message: "POST Sincronizar!",
                createdUser: result
            });
        }
    )
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

// Ver usuarios Por Id
exports.dataCsv_verById = (req, res, next) => {
    const id = req.params.id;
    dataCsv.findById(id)
    .exec()
    .then(doc => {
        console.log("Desde Base de datos", doc);
        if(doc){
            res.status(200).json(doc);
        } else{
            res.status(404)
            .json({
                message: "No se encontro data con ese ID"
            });
        }
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
    });
};

// Eliminar usuario de La BD por Id
exports.dataCsv_deleteUser = (req, res, next) => {
    const id = req.params.id;
    dataCsv.remove({ _id: id })
      .exec()
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
};

// Ruta Patch User BD
exports.dataCsv_patchUser = (req, res, next) => {
    const id = req.params.id;
    const updateOps = {};
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value;
    }
    dataCsv.update({ _id: id }, { $set: updateOps })
      .exec()
      .then(result => {
        console.log(result);
        res.status(200).json(result);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
};