const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');



const dataCsvRoutes = require('./api/routes/dataCsv');
const userRoutes = require('./api/routes/user');
const workplaceRoutes = require('./api/routes/workplace');

mongoose.connect("mongodb+srv://admin:admin@cluster0-5nk33.mongodb.net/test?retryWrites=true",
    { 
        useNewUrlParser: true 
    }
);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin","*");
    res.header("Access-Control-Allow-Headers","*");
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    return next();
});

app.use('/datacsv', dataCsvRoutes);
app.use('/user', userRoutes);
app.use('/workplace', workplaceRoutes);

app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) =>{
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

module.exports = app;